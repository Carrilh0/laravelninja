@extends('layout/principal')

@section('conteudo')


    <h1>Novo produto</h1>
    <form action="{{action('ProdutoController@atualiza', $produto->id)}}" method='POST'>
        <div class='form-group'>
            <label >Nome</label>
            <input name='nome' value="{{$produto->nome}}" class='form-control'/>
        </div>
        <div class='form-group'>
            <label>Descricao</label>
            <input name='descricao' value="{{$produto->descricao}}" class='form-control'/>
        </div>
        <div class='form-group'>
            <label>Valor</label>
            <input name='valor' value="{{$produto->valor}}" class='form-control'/>
        </div>
        <div class='form-group'>
            <label>Quantidade</label>
            <input name='quantidade' value="{{$produto->quantidade}}" class='form-control' type="number"/>
        </div>
        <button class='btn btn-primary btn-block' type="submit">Submit</button>
        
        
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        
    </form>

@endsection