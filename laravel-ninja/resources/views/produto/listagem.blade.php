@extends('layout/principal')

@section('conteudo')
        @if(empty($produtos))
            <div class="alert alert-danger">
            Você não tem nenhum produto cadastrado.
            </div>
        @else
        <h1>Listagem de produtos</h1>
            <table class="table table-striped table-bordered table-hover">
            
            @foreach ($produtos as $p)
            <tr class="{{$p->quantidade <= 1 ? 'text-danger' : ''}}"> <!-- CONDICIONAL PARA MOSTRAR VERMELHO SE QUANTIDADE FOR MENOR OU IGUAL A 1 -->
                
                <td>{{ $p->nome }} </td>
                <td>{{ $p->valor }} </td>
                <td>{{ $p->descricao }} </td>
                <td>{{ $p->quantidade }} </td>
                <td><a href="/produtos/mostra/{{$p->id}}"> Visualizar </a> <!-- PASSAR PARAMETRO PARA URL -->
                <td><a href="{{action('ProdutoController@remove', $p->id)}}"> Remover </a></td>
                <td><a href="{{action('ProdutoController@editar', $p->id)}}"> Editar </a></td>
            
            </tr>
            @endforeach
        @endif
            </table>
                
                
            <span class="badge badge-danger float-right">
            Um ou menos itens no estoque
            </span>
            
        
        
        @if(old('nome')) <!--dessa forma fica explícito que estamos exibindo um valor da requisição anterior. -->
            <div>                
                <span class="alert alert-success float-none">
                {{old('nome')}} adicionado com sucesso
                </span>               
            </div>
        @endif


@endsection