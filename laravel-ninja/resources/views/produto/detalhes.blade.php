@extends('layout/principal')

@section('conteudo')
            <h1>Detalhes do produto: {{ $resposta->nome }} </h1>
            <ul>
            <li>
            <b>Valor:</b> R$ {{ $resposta->valor }}
            </li>
            <li>
            <b>Descrição:</b> {{ $resposta->descricao }}
            </li>
            <li>
            <b>Quantidade em estoque:</b> {{ $resposta->quantidade }}
            </li>
            </ul>
@endsection