<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<link href="/css/custom.css" rel="stylesheet">
<title>Controle de estoque</title>
</head>
    <body>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="/produtos">
                        Estoque Laravel
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{action('ProdutoController@listar')}}">Listagem</a></li>
                    <li><a href="{{action('ProdutoController@novo')}}">Novo</a></li>
                    </ul>
                    </div>
            </nav>
        @yield('conteudo')
        <footer class="footer">
        <p>© Livro de Laravel da Casa do Código.</p>
        </footer>
        </div>
    </body>
</html>