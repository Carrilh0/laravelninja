<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdutosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [        //filtros para os inputs
           'nome' => 'required|max:100',        //requer no maximo 100 caracters
           'descricao' => 'required|max:255',   //requer no maximo 250 caracters
           'valor' => 'required|numeric'        //requer campo numerico
        ];
    }

    public function messages(){
        return [
            //'nome.max' => 'Campo :attribute não preenchido', //retorna mensagem para todos os campos
            //'valor.required' => 'Atributo :attribute deve ser numero'  //retorna mensagem para campo especifico
        ];
    }
}
