<?php

namespace App\Http\Controllers;
use Request;
use App\Produto;
use DB;
use Validator;
use App\Http\Requests\ProdutosRequest;


class ProdutoController extends Controller
{
    
    public function __construct()       //construtor para checar login
    {
        $this->middleware('auth');
    }
    
    public function listar(){
        
        $produtos = Produto::all();
        
        return view('produto/listagem', compact('produtos'));
    }

    public function mostra($id){ //pega parametros da rota
        
        //$id = Request::input('id','0'); //pega parametros da url com padrão 0
        //$id = Request::route('id','0'); //pega parametros da rota com o padrão 0

        $resposta = Produto::find($id); 
        
        if(empty($resposta)){
           
            return "Esse produto não existe";

        }else{

        return view('produto/detalhes', compact('resposta'));
        }
    }

    public function novo(){
        return view('produto/novo');
    }

    public function adiciona(ProdutosRequest $request){
        
        Produto::create(Request::all()); //salva os dados dos inputs diretamente no banco
        return redirect()->action('ProdutoController@listar')->withInput(Request::only('nome'));
        
                //-------------------------------VALIDADOR---------------------------------------------//        
        //$validator = Validator::make(['nome' => Request::input('nome')],['nome'=> 'required|min:5']);
        //if ($validator->fails()){
        //    return redirect()->action('ProdutoController@novo');
        //}
        //$menssagem = $validator->messages();
        
        //-------------------------------------------------------------------------// 
        //$all = Request::all();     //Recupera todos os valores dos inputs
       // $only = Request::only('nome', 'valor');  //recupera apenas inputs especificos
        
        //-------------------------------------------------------------------------//
        //$values = Request::all();
        //$produto = new Produto($values);
        //$produto->save();                   //INSERT COM ELOQUENT                    
        
       //-------------------------------------------------------------------------// 
        //$produto->nome = Request::input('nome');
        //$produto->descricao = Request::input('descricao');
        //$produto->valor = Request::input('valor');
        //$produto->quantidade = Request::input('quantidade');

        //DB::insert('insert into produtos (nome,descricao,valor,quantidade) 
        //values (?,?,?,?)',array($nome,$descricao,$valor,$quantidade));
        //-------------------------------------------------------------------------//  
        //return implode(',',array($nome,$descricao,$valor,$quantidade)); //O implode fará com que cada um dos valores seja impresso separado por vírgula
        
        //return redirect('/produtos')->withInput(); //o método withInput depois do redirect e pronto, todos os parâmetros serão mantidos. 

        //return redirect('/produtos')->withInput(Request::only('nome')); //o método withInput(Request::only('nome')) Retorna apenas um valor especifico

        //Assim ele está dizendo explicitamente qual o método que quer redirecionar, independente de sua URI. Se decidirmos no futuro mudar a rota desse método, precisaríamos lembrar de mudar todos os lugares que faziam redirect.
    }

    public function listaJson(){
       
        $produtos = Produto::all();
        return response()->json($produtos); //lista uma pagina json com o array acima

        //return response()->download($caminhoParaUmArquivo); //Acessar um método com esse retorno resultaria no download do arquivo presente no caminho especificado
    }

    
    public function remove($id){
        
        $produto = Produto::find($id); //especifica qual elemento deve ser removido apartir do id
        $produto->delete();             //remove o campo

        return redirect()->action('ProdutoController@listar');
    }

   
    public function editar($id){
        $produto = Produto::find($id);  //especifica qual elemento deve ser editado apartir do id
        return view('produto/editar',compact('produto')); //envia o id para o form junto com os valores
    }
    
    
    public function atualiza($id){
        
        $params = Request::all();       //pega parametros dos inputs da pagina de edit
        $produto = Produto::find($id);  //especifica qual elemento deve ser editado apartir do id
        $produto->update($params);      //atualiza alterações baseadas no id

        return redirect()->action('ProdutoController@listar')->withInputs(Request::only('nome'));
    }
}
